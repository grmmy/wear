package wear_1.grmm.org.myapplication;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.FragmentGridPagerAdapter;
import android.support.wearable.view.GridPagerAdapter;
import android.support.wearable.view.GridViewPager;

public class PickerActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picker);
        final GridViewPager pager = (GridViewPager) findViewById(R.id.pager);
        pager.setAdapter(new SampleGridPagerAdapter(this, getFragmentManager()));
    }

    public static class SampleGridPagerAdapter extends FragmentGridPagerAdapter {

        private static final int[] COLORS = new int[]{Color.RED, Color.BLACK, Color.BLUE};
        private final Context mContext;

        public SampleGridPagerAdapter(Context ctx, FragmentManager fm) {
            super(fm);
            mContext = ctx;
        }

        @Override
        public Fragment getFragment(int i, int i1) {
            return CardFragment.create("Title " + i + " " + i1, "body", android.R.drawable.ic_delete);
        }

        @Override
        public int getRowCount() {
            return 3;
        }

        @Override
        public int getColumnCount(int i) {
            return 5;
        }

        // Obtain the background image for the row
        @Override
        public Drawable getBackgroundForRow(int row) {
            return new ColorDrawable(COLORS[row % COLORS.length]);
        }

        // Obtain the background image for the specific page
        @Override
        public Drawable getBackgroundForPage(int row, int column) {
            if( row == 2 && column == 1) {
                // Place image at specified position
                return mContext.getResources().getDrawable(R.drawable.background);
            } else {
                // Default to background image for row
                return GridPagerAdapter.BACKGROUND_NONE;
            }
        }

    };

}
