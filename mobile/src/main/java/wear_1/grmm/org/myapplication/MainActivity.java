package wear_1.grmm.org.myapplication;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.RemoteInput;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String COUNT_KEY = "COUNT_KEY";
    private GoogleApiClient mGoogleApiClient;
    private static int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        Log.d(TAG, "onConnected: " + connectionHint);
                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                        Log.d(TAG, "onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Log.d(TAG, "onConnectionFailed: " + result);
                    }
                })
                // Request access only to the Wearable API
                .addApi(Wearable.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    public void showSimpleNotification(View view) {
        int notificationId = 001;

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                        .setContentTitle("My notification")
                        .setContentText("Hello World!");

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(notificationId, mBuilder.build());
    }

    public void showSimpleNotificationWithIntent(View view) {
        int notificationId = 002;

        Intent viewIntent = new Intent(this, OpenEventActivity.class);
        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(this, 0, viewIntent, 0);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(android.R.drawable.alert_dark_frame)
                        .setContentTitle("Notification with action")
                        .setContentText("My text")
                        .setContentIntent(viewPendingIntent);

        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);

        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    public void showSimpleNotificationWithIntentAndAction(View view) {
        int notificationId = 003;

        Intent viewIntent = new Intent(this, OpenEventActivity.class);
        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(this, 0, viewIntent, 0);

        Intent actionIntent = new Intent(this, NotificationActionActivity.class);
        PendingIntent actionPendingIntent =
                PendingIntent.getActivity(this, 0, actionIntent, 0);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(android.R.drawable.alert_dark_frame)
                        .setContentTitle("Notification with action")
                        .setContentText("My text")
                        .setContentIntent(viewPendingIntent)
                        .addAction(new NotificationCompat.Action(android.R.drawable.ic_dialog_email, "New Email", actionPendingIntent));

        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);

        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    public void showSimpleNotificationWithIntentAndWearAction(View view) {
        int notificationId = 004;

        Intent viewIntent = new Intent(this, OpenEventActivity.class);
        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(this, 0, viewIntent, 0);

        Intent actionIntent = new Intent(this, NotificationActionActivity.class);
        PendingIntent actionPendingIntent =
                PendingIntent.getActivity(this, 0, actionIntent, 0);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(android.R.drawable.alert_dark_frame)
                        .setContentTitle("Notification with action")
                        .setContentText("My text")
                        .setContentIntent(viewPendingIntent)
                        .addAction(new NotificationCompat.Action(android.R.drawable.ic_dialog_email, "New Email", actionPendingIntent))
                        .extend(new NotificationCompat.WearableExtender().addAction(new NotificationCompat.Action(android.R.drawable.ic_menu_call, "Call", actionPendingIntent)));

        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);

        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    public void showBigNotification(View view) {
        int notificationId = 005;

        NotificationCompat.BigTextStyle bigStyle = new NotificationCompat.BigTextStyle();
        bigStyle.bigText("Except Lorem Ipcum, there are many different big texts, for example this one. It is not really big, you know, like BIG, it is just big enough for our needs. So lets use it and have fun!");

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(android.R.drawable.alert_dark_frame)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notif_background))
                        .setContentTitle("Notification with action")
                        .setContentText("My text")
                        .setStyle(bigStyle);

        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);

        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    public void showWearFeaturedNotification(View view) {
        int notificationId = 006;

        NotificationCompat.Builder pageBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(android.R.drawable.alert_dark_frame)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notif_background));

        final NotificationCompat.WearableExtender extender = new NotificationCompat.WearableExtender();
        extender.addPage(pageBuilder.setContentTitle("page #2").setContentText("Seeeeeecond page!").build());
        extender.addPage(pageBuilder.setContentTitle("page #3").setContentText("Thiiiiird pageeee!").build());
        extender.addPage(pageBuilder.setContentTitle("page #4").setContentText("Hell yeah!").build());
        extender.addPage(pageBuilder.setContentTitle("page #5").setContentText("Amazing!").build());
        extender.addPage(pageBuilder.setContentTitle("page #6").setContentText("Amazing!").build());
        extender.addPage(pageBuilder.setContentTitle("page #7").setContentText(" (╯°□°）╯").build());
        extender.addPage(pageBuilder.setContentTitle("page #8").setContentText("\\(°□°)/").build());
        extender.addPage(pageBuilder.setContentTitle("page #9").setContentText("ヽ(。_°)ノ ").build());

//        extender.setHintHideIcon(true);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(android.R.drawable.alert_dark_frame)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notif_background))
                        .setContentTitle("page 1")
                        .setContentText("First page")
                        .extend(extender);

        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);

        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    public void showVoiceInputNotification(View view) {
        int notificationId = 007;
        String replyLabel = "Reply label";

        RemoteInput remoteInput = new RemoteInput.Builder(VoiceReplyActivity.EXTRA_VOICE_REPLY)
                .setLabel(replyLabel)
                .setChoices(getResources().getStringArray(R.array.reply_choices))
                .build();

        Intent actionIntent = new Intent(this, VoiceReplyActivity.class);
        PendingIntent actionPendingIntent = PendingIntent.getActivity(this, 1, actionIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        final NotificationCompat.Action action = new NotificationCompat.Action
                .Builder(android.R.drawable.ic_dialog_email, "Email by Voice", actionPendingIntent)
                .addRemoteInput(remoteInput)
                .build();
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(android.R.drawable.alert_dark_frame)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notif_background))
                        .setContentTitle("Title")
                        .addAction(action)
                        .setContentText("Text");

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    final static String GROUP_KEY_EMAILS = "group_key_emails";

    public void showStackedNotification(View view) {
        int notificationId = 010;
        int notificationSummaryId = 100;

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(android.R.drawable.alert_dark_frame)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notif_background))
                        .setGroup(GROUP_KEY_EMAILS);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        final NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        for(int notificationIndex = 0; notificationIndex < 10; notificationIndex++) {
            final String title = "Title #" + notificationIndex;
            final String text = "Notification text #" + notificationIndex;
            inboxStyle.addLine(title + ": " + text);
            notificationBuilder
                    .setContentTitle(title)
                    .setContentText(text);
            notificationManager.notify(notificationId + notificationIndex, notificationBuilder.build());
        }

        // Create an InboxStyle notification
        NotificationCompat.Builder summaryNotification = new NotificationCompat.Builder(this)
                .setContentTitle("10 messages")
                .setSmallIcon(android.R.drawable.alert_dark_frame)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notif_background))
                .setStyle(inboxStyle
                        .setBigContentTitle("10 new messages")
                        .setSummaryText("test@gmail.com"))
                .setGroup(GROUP_KEY_EMAILS)
                .setGroupSummary(true);

        notificationManager.notify(notificationSummaryId, summaryNotification.build());

    }

    public void increaseCounter(View view) {
        PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/count");
        putDataMapReq.getDataMap().putInt(COUNT_KEY, count++);
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        PendingResult<DataApi.DataItemResult> pendingResult =
                Wearable.DataApi.putDataItem(mGoogleApiClient, putDataReq);
        pendingResult.setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
            @Override
            public void onResult(@NonNull DataApi.DataItemResult dataItemResult) {
                Log.d(TAG, dataItemResult.getDataItem().toString());
            }
        });
    }

    public void startActivity(final View view) {
        Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult getConnectedNodesResult) {
                for (Node node : getConnectedNodesResult.getNodes()) {
                    sendMessage(node.getId(), view.getId() == R.id.main_activity_start ? START_ACTIVITY_PATH : COUNTER_ACTIVITY_PATH);
                }
            }
        });

    }

    public static final String START_ACTIVITY_PATH = "/start/MainActivity";
    public static final String COUNTER_ACTIVITY_PATH = "/start/CounterActivity";


    private void sendMessage(String node, String path) {
        Wearable.MessageApi.sendMessage(mGoogleApiClient , node , path , new byte[0]).setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
            @Override
            public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                if (!sendMessageResult.getStatus().isSuccess()) {
                    Log.e("GoogleApi", "Failed to send message with status code: "
                            + sendMessageResult.getStatus().getStatusCode());
                }
            }
        });
    }
}


